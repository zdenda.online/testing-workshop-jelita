package online.zdenda.ws.tc.characters

import online.zdenda.ws.tc.characters.tools.CupsCard

/**
 * Represents Chandler Bing.
 */
class Chandler {

    /**
     * Plays a card for 'Cups' game.
     *
     * @return a random card
     */
    fun playCupsCard(): CupsCard {
        return CupsCard.newRandomCard()
    }

    /**
     * Gets a flag whether did cleaning in the apartment and didn't return anything back in place (before Monica returns).
     *
     * @return true if did otherwise false
     */
    fun didCleaning(): Boolean {
        return Math.random() < 0.3
    }
}