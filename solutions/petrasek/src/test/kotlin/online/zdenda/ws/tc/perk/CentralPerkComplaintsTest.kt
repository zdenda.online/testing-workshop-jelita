package online.zdenda.ws.tc.perk

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * Tests [CentralPerk.processComplaint]
 */
class CentralPerkComplaintsTest {

    private val perk = CentralPerk()

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = Coffee(CoffeeType.LONG_BLACK, 10, 10)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is not equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = Coffee(CoffeeType.ESPRESSO, 7, 25)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = Coffee(CoffeeType.ESPRESSO, 10, 100)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }
}