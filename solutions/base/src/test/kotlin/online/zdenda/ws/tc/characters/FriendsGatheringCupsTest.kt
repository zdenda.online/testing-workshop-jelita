package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.characters.tools.CupsCard
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class FriendsGatheringCupsTest {

    @ParameterizedTest
    @MethodSource("paramsProvider")
    fun `playing 'Cups' cards game with Joey `(joeyValue: Int, joeyColor: CupsCard.Color, chandlerValue: Int, chandlerColor: CupsCard.Color, joeyShouldWin: Boolean) {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
        given(joey.playCupsCard()).willReturn(CupsCard(joeyValue, joeyColor))
        given(chandler.playCupsCard()).willReturn(CupsCard(chandlerValue, chandlerColor))

        // when
        val joeyWins = friendsGathering.playCupsCardsTurn()

        // then
        assertEquals(joeyWins, joeyShouldWin)
        verify(joey).playCupsCard()
        verify(chandler).playCupsCard()
    }

    companion object {
        @JvmStatic
        fun paramsProvider(): Stream<Arguments> {
            return Stream.of(
                    Arguments.of(10, CupsCard.Color.DIAMOND, 10, CupsCard.Color.DIAMOND, false),
                    Arguments.of(3, CupsCard.Color.DIAMOND, 10, CupsCard.Color.DIAMOND, true),
                    Arguments.of(10, CupsCard.Color.DIAMOND, 3, CupsCard.Color.DIAMOND, true),
                    Arguments.of(3, CupsCard.Color.SPADE, 10, CupsCard.Color.DIAMOND, true),
                    Arguments.of(10, CupsCard.Color.SPADE, 7, CupsCard.Color.DIAMOND, true),
                    Arguments.of(10, CupsCard.Color.DIAMOND, 10, CupsCard.Color.SPADE, true),
                    Arguments.of(10, CupsCard.Color.SPADE, 10, CupsCard.Color.DIAMOND, true)
            )
        }
    }
}