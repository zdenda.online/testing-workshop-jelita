package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.createCoffee
import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Mock

/**
 * Tests [CentralPerk.processComplaint]
 */
class CentralPerkComplaintsTest {
    @Mock
    private lateinit var coffeeStockMock: CoffeeStock
    private val perk = CentralPerk(coffeeStockMock)

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = createCoffee(CoffeeType.LONG_BLACK, 10, 10)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is not equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = createCoffee(CoffeeType.ESPRESSO, 7, 25, 5, 5.5)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = createCoffee(CoffeeType.ESPRESSO, 10, 100)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }
}