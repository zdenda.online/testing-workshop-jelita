package online.zdenda.ws.tc.perk.stock

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class MilkStockTest {
    @Test
    fun `when trying to take more than initial capacity should return false`() {
        // given
        val stock = MilkStock(10)

        // when
        val taken = stock.take(11)

        // then
        Assertions.assertFalse(taken)
    }

    @Test
    fun `when trying to take more than remaining capacity should return false`() {
        // given
        val stock = MilkStock(15)

        // when
        stock.take(11)
        val taken = stock.take(11)

        // then
        Assertions.assertFalse(taken)
    }

    @Test
    fun `when trying to take less than remaining capacity should return true`() {
        // given
        val stock = MilkStock(55)

        // when
        stock.take(10)
        stock.take(10)
        val taken = stock.take(10)

        // then
        Assertions.assertTrue(taken)
    }

    @Test
    fun `when trying to take remaining capacity should return true`() {
        // given
        val stock = MilkStock(10)

        // when
        val taken = stock.take(10)

        // then
        Assertions.assertTrue(taken)
    }
}