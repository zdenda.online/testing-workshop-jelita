package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class FriendsGatheringLoveTest {
    @Test
    fun `attempt to couple Ross and Rachel when no one want to love returns false`() {
        // given
        val friendsGathering = initFriendsGathering(false, false)

        // when
        val areCouple = friendsGathering.attemptToCoupleRossAndRachel()

        // then
        Assertions.assertFalse(areCouple)
    }

    @Test
    fun `attempt to couple Ross and Rachel when only Rachel wants to love Ross returns false`() {
        // given
        val friendsGathering = initFriendsGathering(false, true)

        // when
        val areCouple = friendsGathering.attemptToCoupleRossAndRachel()

        // then
        Assertions.assertFalse(areCouple)
    }

    @Test
    fun `attempt to couple Ross and Rachel when only Ross wants to love Rachel returns false`() {
        // given
        val friendsGathering = initFriendsGathering(true, false)

        // when
        val areCouple = friendsGathering.attemptToCoupleRossAndRachel()

        // then
        Assertions.assertFalse(areCouple)
    }

    @Test
    fun `attempt to couple Ross and Rachel when both Ross and Rachel wants to love returns true`() {
        // given
        val friendsGathering = initFriendsGathering(true, true)

        // when
        val areCouple = friendsGathering.attemptToCoupleRossAndRachel()

        // then
        Assertions.assertTrue(areCouple)
    }

    private fun initFriendsGathering(isRossWillingToLoveRachel: Boolean, isRachelWillingToLoveRoss: Boolean): FriendsGathering {
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()

        given(ross.isWillingToLoveRachel()).willReturn(isRossWillingToLoveRachel)
        given(rachel.isWillingToLoveRoss()).willReturn(isRachelWillingToLoveRoss)

        return FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
    }
}