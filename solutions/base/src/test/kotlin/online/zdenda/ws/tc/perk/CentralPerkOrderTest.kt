package online.zdenda.ws.tc.perk

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`

/**
 * Tests [CentralPerk.order]
 */
class CentralPerkOrderTest {
    @Mock
    private val coffeeStockMock: CoffeeStock = mock()
    private val perk = CentralPerk(coffeeStockMock)

    @Test
    fun `ordering not offered coffee throws IllegalArgumentException`() {
        // when
        val orderFunction: () -> Unit = { perk.order(CoffeeType.LONG_BLACK) }

        // then
        Assertions.assertThrows(IllegalArgumentException::class.java, orderFunction)
    }

    @Test
    fun `ordering offered coffee with not enough coffee stock returns null`() {
        // when
        `when`(coffeeStockMock.take(any(), any())).thenReturn(false)
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertNull(coffee)
    }

    @Test
    fun `ordering offered coffee with enough coffee stock returns coffee of such type`() {
        // when
        `when`(coffeeStockMock.take(any(), any())).thenReturn(true)
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertEquals(CoffeeType.ESPRESSO, coffee!!.type)
    }
}