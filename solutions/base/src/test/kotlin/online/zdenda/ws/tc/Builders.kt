package online.zdenda.ws.tc

import online.zdenda.ws.tc.perk.Coffee
import online.zdenda.ws.tc.perk.CoffeeType

fun createCoffee(type: CoffeeType = CoffeeType.ESPRESSO,
                 coffeeGrams: Int = 10,
                 waterMilliliters: Int = 20,
                 milkMillilitres: Int = 15,
                 price: Double = 1.1): Coffee {

    return Coffee(type, coffeeGrams, waterMilliliters, milkMillilitres, price)
}