package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import online.zdenda.ws.tc.characters.tools.Guitar
import online.zdenda.ws.tc.characters.tools.GuitarString
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringSmellyCatTest {

    @Test
    fun `letting Phoebe to sing Smelly Cat when any guitar string is not tuned returns false (nothing is singed)`() {
        // given
        val friendsGathering = initFriendsGathering(false)

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertFalse(didPhoebeSing)
    }

    @Test
    fun `letting Phoebe to sing Smelly Cat when all guitar strings are tuned returns true`() {
        // given
        val friendsGathering = initFriendsGathering(true)

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertTrue(didPhoebeSing)
    }

    private fun initFriendsGathering(guitarIsTuned: Boolean): FriendsGathering {
        val guitarString: GuitarString = mock()
        val guitar: Guitar = mock()

        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()

        given(guitarString.isTuned).willReturn(guitarIsTuned)
        given(guitar.strings).willReturn(arrayOf(guitarString))
        given(phoebe.guitar).willReturn(guitar)
        return FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
    }
}