package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class FriendsCleaningTest {
    @ParameterizedTest
    @MethodSource("paramsProvider")
    fun `monica should clean when anyone did a mess`(isOttomanMoved: Boolean,
                                               isSpaghettiOnFloor: Boolean,
                                               isCoffeeTableMessy: Boolean,
                                               areFridgeMagnetsInPlace: Boolean,
                                               isPhoebePregnant: Boolean,
                                               monicaShouldClean: Boolean) {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()

        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)

        given(rachel.movedGreenOttoman()).willReturn(isOttomanMoved)
        given(joey.droppedSpaghettiOnTheFloor()).willReturn(isSpaghettiOnFloor)
        given(ross.marcelPeedInCoffeeTable()).willReturn(isCoffeeTableMessy)
        given(chandler.didCleaning()).willReturn(areFridgeMagnetsInPlace)
        given(phoebe.isPregnant()).willReturn(isPhoebePregnant)

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(monica, if (monicaShouldClean) atLeastOnce() else never() ).doCleaning()
    }

    companion object {
        @JvmStatic
        fun paramsProvider(): Stream<Arguments> {
            return Stream.of(
                    Arguments.of(true, false, false, false, false, true),
                    Arguments.of(false, true, false, false, false, true),
                    Arguments.of(false, false, true, false, false, true),
                    Arguments.of(false, false, false, true, false, true),
                    Arguments.of(false, false, false, false, true, true),
                    Arguments.of(true, true, true, true, true, true),
                    Arguments.of(false, false, false, false, false, false)
            )
        }
    }
}