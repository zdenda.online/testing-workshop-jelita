package online.zdenda.ws.tc.perk.stock

class MilkStock(initialCapacity: Int) {
    private var remaining = initialCapacity

    fun take(milliliters: Int): Boolean {
        if (remaining < milliliters) {
            return false
        }

        remaining -= milliliters
        return true
    }
}