package online.zdenda.ws.tc.characters

/**
 * Represents Monica Geller.
 */
class Monica {

    /**
     * Does cleaning of the apartment.
     */
    fun doCleaning() {
        println("I will have to clean up after myself (vacuum bigger vacuum cleaner with smaller one)")
    }
}