package online.zdenda.ws.tc.perk

enum class CoffeeType {
    ESPRESSO,
    LONG_BLACK,
    AMERICANO,
    LATTE
}