package online.zdenda.ws.tc.characters

import online.zdenda.ws.tc.characters.tools.CupsCard

/**
 * Represents Joey Tribbiani.
 */
class Joey {

    /**
     * Plays a card for 'Cups' game.
     *
     * @return a random card
     */
    fun playCupsCard(): CupsCard {
        return CupsCard.newRandomCard()
    }

    /**
     * Gets a flag whether dropped spaghetti on the floor to show Rachel it does not matter.
     *
     * @return true if dropped it otherwise false
     */
    fun droppedSpaghettiOnTheFloor(): Boolean {
        return Math.random() < 0.4
    }
}