package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock

/**
 * Tests [CentralPerk.processComplaint]
 */
class CentralPerkComplaintsTest {


    @Mock
    private lateinit var stock: CoffeeStock
    @InjectMocks
    private lateinit var perk: CentralPerk

    private fun aCoffee(type: CoffeeType = CoffeeType.ESPRESSO,
                        coffeeGrams: Int = 7,
                        waterMilliliters: Int = 25,
                        milkMillilitres: Int = 5,
                        price: Double = 2.5): Coffee {
        return Coffee(type, coffeeGrams, waterMilliliters, milkMillilitres, price)
    }

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = aCoffee(CoffeeType.LONG_BLACK, 10, 10)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is not equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = aCoffee(CoffeeType.ESPRESSO, 7, 25)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = aCoffee(CoffeeType.ESPRESSO, 10, 100)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }
}