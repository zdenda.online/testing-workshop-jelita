package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.characters.tools.CupsCard
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringCupsTest {

    @Test
    fun `playing 'Cups' cards game with Joey getting 10 of spades and Chandler 3 of diamond results in Joey's win`() {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
        given(joey.playCupsCard()).willReturn(CupsCard(10, CupsCard.Color.SPADE))
        given(chandler.playCupsCard()).willReturn(CupsCard(3, CupsCard.Color.DIAMOND))

        // when
        val joeyWins = friendsGathering.playCupsCardsTurn()

        // then
        assertTrue(joeyWins)
        verify(joey).playCupsCard()
        verify(chandler).playCupsCard()
    }
}