package online.zdenda.ws.tc.perk

enum class CoffeeType {
    ESPRESSO,
    LATTE,
    LONG_BLACK,
    AMERICANO,
}