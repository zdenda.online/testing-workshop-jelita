package online.zdenda.ws.tc.characters

import online.zdenda.ws.tc.RandomGenerator

/**
 * Represents Rachel Green.
 */
class Rachel {

    /**
     * @return a flag whether Rachel is willing to accept love from Ross.
     * Note that this is kind of random and with little chance.
     */
    fun isWillingToLoveRoss(): Boolean {
        return Math.random() < 0.2 // in only 20% percents has Ross the chance
    }

    /**
     * Gets a flag whether moved the green ottoman.
     *
     * @return true if moved otherwise false
     */
    fun movedGreenOttoman(): Boolean {
       var generator = RandomGenerator()
        return generator.random() < 0.2
    }
}