package online.zdenda.ws.tc.characters

import online.zdenda.ws.tc.RandomGenerator

/**
 * Represents Ross Geller.
 */
class Ross {

    /**
     * @return a flag whether Ross is willing to accept love from Rachel.
     * That is kind of a dumb question as it happens almost always
     */
    fun isWillingToLoveRachel(): Boolean {
        return Math.random() < 0.95 // almost sure thing
    }

    /**
     * Gets a flag whether Marcel monkey urinated into coffee table.
     *
     * @return true if he did it otherwise false
     */
    fun marcelPeedInCoffeeTable(): Boolean {
        var generator = RandomGenerator()
        return generator.random() < 0.1
    }
}