package online.zdenda.ws.tc.perk

data class Coffee(val type: CoffeeType,
                  val coffeeGrams: Int,
                  val waterMilliliters: Int,
                  val milkMillilitres: Int,
                  val price: Double
)