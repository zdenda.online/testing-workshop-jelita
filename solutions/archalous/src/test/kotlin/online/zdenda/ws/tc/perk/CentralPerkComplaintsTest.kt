package online.zdenda.ws.tc.perk

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * Tests [CentralPerk.processComplaint]
 */
class CentralPerkComplaintsTest {

    private val perk = CentralPerk()

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = aCoffee(CoffeeType.LONG_BLACK, 10, 10, 10)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = aCoffee(CoffeeType.ESPRESSO, 7, 25, 20)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is not equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = aCoffee(CoffeeType.ESPRESSO, 10, 100, 50)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }


}

private fun aCoffee(type: CoffeeType = CoffeeType.AMERICANO, coffeeGrams: Int = 10, waterMilliliters: Int  = 10, milnMilliliters: Int  = 10, price: Double = 5.0): Coffee {
    return Coffee(type, coffeeGrams, waterMilliliters, milnMilliliters, price)
}
