package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.characters.tools.CupsCard
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Mockito.times

class FriendsGatheringCupsTest {

    @Test
    fun `playing 'Cups' cards game with Joey getting 10 of spades and Chandler 3 of diamond results in Joey's win`() {

       val cardsCombinations = arrayOf(
                arrayOf(CupsCard(10, CupsCard.Color.SPADE), CupsCard(3, CupsCard.Color.DIAMOND)),
                arrayOf(CupsCard(3, CupsCard.Color.SPADE), CupsCard(10, CupsCard.Color.DIAMOND)),
                arrayOf(CupsCard(4, CupsCard.Color.SPADE), CupsCard(4, CupsCard.Color.DIAMOND))
        )
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)


        for (i in cardsCombinations) {
            given(joey.playCupsCard()).willReturn(i.get(0))
            given(chandler.playCupsCard()).willReturn(i.get(1))
            assertTrue( friendsGathering.playCupsCardsTurn())
        }

        verify(joey, times(3)).playCupsCard();
        verify(chandler, times(3)).playCupsCard();

    }
}