package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.RandomGenerator
import online.zdenda.ws.tc.characters.tools.Guitar
import org.junit.jupiter.api.Test
import org.mockito.Mockito.times

class FriendsCleaningTest {

    @Test
    fun `Monice cleaning test`() {


        val guitar: Guitar = mock()
        val rachel = Rachel()
        val monica: Monica = mock()
        val phoebe = Phoebe(guitar)
        val joey = Joey()
        val chandler = Chandler()
        val ross = Ross()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
        val randomGenerator: RandomGenerator = mock();
        given(randomGenerator.random()).willReturn(0.0)

        friendsGathering.checkIfMonicaShouldDoCleaning();

        verify(monica, times(1)).doCleaning();

    }
}