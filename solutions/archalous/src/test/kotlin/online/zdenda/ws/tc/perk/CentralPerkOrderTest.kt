package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock

/**
 * Tests [CentralPerk.order]
 */
class CentralPerkOrderTest {

    private val perk = CentralPerk()

    @Test
    fun `ordering not offered coffee throws IllegalArgumentException`() {
        // when
        val orderFunction: () -> Unit = { perk.order(CoffeeType.LONG_BLACK) }

        // then
        Assertions.assertThrows(IllegalArgumentException::class.java, orderFunction)
    }

    @Test
    fun `ordering offered coffee with not enough coffee stock returns null`() {

        val coffeeStock: CoffeeStock = mock()
        given(coffeeStock.take(CoffeeType.ESPRESSO,7)).willReturn(true)

        // when
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertNull(coffee)
    }

    @Test
    fun `ordering offered coffee with enough coffee stock returns coffee of such type`() {
        // when
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertEquals(CoffeeType.ESPRESSO, coffee!!.type)
    }
}