package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import online.zdenda.ws.tc.characters.tools.Guitar
import online.zdenda.ws.tc.characters.tools.GuitarString
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringSmellyCatTest {


    @Test
    fun `letting Phoebe to sing Smelly Cat when any guitar string is not tuned returns false (nothing is singed)`() {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
        val guitar: Guitar = mock()

        val guitarStrings = arrayOf(GuitarString('a', true), GuitarString('b', false))
        given(guitar.strings).willReturn(guitarStrings)
        given(phoebe.guitar).willReturn(guitar)

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertFalse(didPhoebeSing)
    }

    @Test
    fun `letting Phoebe to sing Smelly Cat when all guitar strings are tuned returns true`() {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
        val guitar: Guitar = mock()

        val guitarStrings = arrayOf(GuitarString('a', true), GuitarString('b', true))
        given(guitar.strings).willReturn(guitarStrings)
        given(phoebe.guitar).willReturn(guitar)

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertTrue(didPhoebeSing)
    }
}