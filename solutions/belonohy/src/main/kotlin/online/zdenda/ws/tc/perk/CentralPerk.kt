package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.perk.stock.CoffeeStock

/**
 * Represents a Central Perk Coffee House.
 */
class CentralPerk(private val coffeeStock: CoffeeStock) {

    private val COFFEE_PROTOTYPES = hashMapOf(
            CoffeeType.ESPRESSO to Coffee(CoffeeType.ESPRESSO, 7, 25, 0, 10.0),
            CoffeeType.AMERICANO to Coffee(CoffeeType.AMERICANO, 7, 100, 0, 13.000)
            // we don't do LONG_BLACK coffees
    )

    /**
     * Orders a new coffee of given type.
     *
     * @return such coffee or null if Central Perk run out of coffee for given type.
     * @throws IllegalArgumentException if ordering coffee that is not offered by Central Perk.
     */
    fun order(type: CoffeeType): Coffee? {
        if (COFFEE_PROTOTYPES[type] == null) throw IllegalArgumentException("We don't offer $type coffees")
        val coffee = COFFEE_PROTOTYPES[type]!!.copy()
        val isCoffeeInStock = coffeeStock.take(type, coffee.coffeeGrams)
        return if (isCoffeeInStock) coffee else null

    }

    /**
     * Processes a complaint from unsatisfied customer.
     *
     * @return true if complaint was justified (coffee differs from Perk's prototype) or false if not.
     */
    fun processComplaint(coffee: Coffee): Boolean {
        val prototype = COFFEE_PROTOTYPES[coffee.type] ?: return false // we don't make these
        return prototype != coffee // we accept only those differing from prototypes
    }
}