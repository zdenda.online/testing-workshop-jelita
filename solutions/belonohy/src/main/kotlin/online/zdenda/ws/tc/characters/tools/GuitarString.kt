package online.zdenda.ws.tc.characters.tools

data class GuitarString(val tone: Char,
                        val isTuned: Boolean)