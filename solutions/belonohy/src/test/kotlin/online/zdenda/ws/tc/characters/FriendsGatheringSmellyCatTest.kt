package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringSmellyCatTest {

    @Test
    fun `letting Phoebe to sing Smelly Cat when any guitar string is not tuned returns false (nothing is singed)`() {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)

        given(phoebe.guitar).willReturn(mock()) // this is not a complete object structure of Phoebe

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertFalse(didPhoebeSing)
    }

    @Test
    fun `letting Phoebe to sing Smelly Cat when all guitar strings are tuned returns true`() {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)

        given(phoebe.guitar).willReturn(mock()) // this is not a complete object structure of Phoebe

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertTrue(didPhoebeSing)
    }
}