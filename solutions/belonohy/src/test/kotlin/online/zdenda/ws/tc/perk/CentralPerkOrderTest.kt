package online.zdenda.ws.tc.perk

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mockito.Mockito

/**
 * Tests [CentralPerk.order]
 */
class CentralPerkOrderTest {

    //private val perk = CentralPerk()

    // https://www.baeldung.com/kotlin-mockito
    private val stockMock = Mockito.mock(CoffeeStock::class.java)
    private val perk      = CentralPerk(stockMock)


    @Test
    fun `ordering not offered coffee throws IllegalArgumentException`() {
        // when
        val orderFunction: () -> Unit = { perk.order(CoffeeType.LONG_BLACK) }

        // then
        Assertions.assertThrows(IllegalArgumentException::class.java, orderFunction)
    }

    @Test
    fun `ordering offered coffee with not enough coffee stock returns null`() {
        // given
        given(stockMock.take(any(), any())).willReturn(false)

        // when
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertNull(coffee)
    }

    @Test
    fun `ordering offered coffee with enough coffee stock returns coffee of such type`() {
        // given
        given(stockMock.take(any(), any())).willReturn(true)
        // when
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertEquals(CoffeeType.ESPRESSO, coffee!!.type)
    }
}