package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.getCoffee
import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Mockito

/**
 * Tests [CentralPerk.processComplaint]
 */
class CentralPerkComplaintsTest {

    //private val perk = CentralPerk()
    // https://www.baeldung.com/kotlin-mockito
    private val stockMock = Mockito.mock(CoffeeStock::class.java)
    private val perk      = CentralPerk(stockMock)

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = getCoffee(CoffeeType.LONG_BLACK, 10, 10, 0);

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is not equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = getCoffee(CoffeeType.ESPRESSO, 7, 25, 0)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = getCoffee(CoffeeType.ESPRESSO, 10, 100)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }
}