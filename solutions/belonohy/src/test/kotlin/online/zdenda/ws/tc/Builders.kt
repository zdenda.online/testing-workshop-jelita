package online.zdenda.ws.tc

import online.zdenda.ws.tc.perk.Coffee
import online.zdenda.ws.tc.perk.CoffeeType

fun getCoffee(typeOfCoffee: CoffeeType = CoffeeType.ESPRESSO, coffeeGrams: Int = 9, waterMilliliters: Int = 50, milkMillilitres: Int = 0, price: Double = 10.0) : Coffee {
    return Coffee(typeOfCoffee, coffeeGrams, waterMilliliters, milkMillilitres, price)
}

