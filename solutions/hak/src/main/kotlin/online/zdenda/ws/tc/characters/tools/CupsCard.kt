package online.zdenda.ws.tc.characters.tools

/**
 * Represents a card from game 'Cups' that Chandler made up to give somehow money to Joey.
 */
data class CupsCard(val value: Int,
                    val color: Color) {

    companion object {
        fun newRandomCard(): CupsCard {
            val value = (Math.random() * 13).toInt() + 2
            val colorValues = Color.values()
            val color = colorValues[(Math.random() * colorValues.size).toInt()]
            return CupsCard(value, color)
        }
    }

    fun equalsTo(cupsCard: CupsCard): Boolean {
        return this.color == cupsCard.color && this.value == cupsCard.value;
    }

    enum class Color {
        DIAMOND, CLUB, HEART, SPADE
    }
}

