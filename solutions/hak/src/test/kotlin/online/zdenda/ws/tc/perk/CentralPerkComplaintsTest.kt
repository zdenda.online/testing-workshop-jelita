package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * Tests [CentralPerk.processComplaint]
 */
class CentralPerkComplaintsTest {

    private val perk = CentralPerk(CoffeeStock())

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = createCoffee()

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is not equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = createCoffee()

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that is equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = createCoffee(CoffeeType.ESPRESSO, 10 , 100)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }
}

// Possible to be created by builder class.
fun createCoffee(type: CoffeeType = CoffeeType.ESPRESSO,
                 coffeeGrams: Int = 7,
                 waterMilliliters: Int = 25,
                 milkMillilitres: Int = 0,
                 price: Double = 20.0): Coffee {
    return Coffee(type, coffeeGrams, waterMilliliters, milkMillilitres, price)
}