package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.characters.tools.CupsCard
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FriendsGatheringCupsTest {

    @Test
    fun `playing 'Cups' cards game`() {
        doCupsCardTest(CupsCard(3, CupsCard.Color.SPADE),CupsCard(10, CupsCard.Color.DIAMOND), true)
        doCupsCardTest(CupsCard(10, CupsCard.Color.SPADE),CupsCard(3, CupsCard.Color.DIAMOND), true)
        doCupsCardTest(CupsCard(10, CupsCard.Color.SPADE),CupsCard(10, CupsCard.Color.DIAMOND), true)
        doCupsCardTest(CupsCard(10, CupsCard.Color.DIAMOND),CupsCard(10, CupsCard.Color.SPADE), true)
        doCupsCardTest(CupsCard(10, CupsCard.Color.DIAMOND),CupsCard(10, CupsCard.Color.DIAMOND), false)
    }
}


fun doCupsCardTest(joeyCupsCard: CupsCard, chandlerCupsCard: CupsCard, shoudJoeyWin: Boolean) {
    // given
    val rachel: Rachel = mock()
    val monica: Monica = mock()
    val phoebe: Phoebe = mock()
    val joey: Joey = mock()
    val chandler: Chandler = mock()
    val ross: Ross = mock()
    val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
    given(joey.playCupsCard()).willReturn(joeyCupsCard)
    given(chandler.playCupsCard()).willReturn(chandlerCupsCard)

    // when
    val joeyWins = friendsGathering.playCupsCardsTurn()

    // then
    assertEquals(shoudJoeyWin, joeyWins)
    verify(joey).playCupsCard()
    verify(chandler).playCupsCard()
}