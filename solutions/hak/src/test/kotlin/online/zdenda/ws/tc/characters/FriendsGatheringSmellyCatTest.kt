package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import online.zdenda.ws.tc.characters.tools.Guitar
import online.zdenda.ws.tc.characters.tools.GuitarString
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringSmellyCatTest {
    private val rachel: Rachel = mock()
    private val monica: Monica = mock()
    private val phoebe: Phoebe = mock()
    private val joey: Joey = mock()
    private val chandler: Chandler = mock()
    private val ross: Ross = mock()
    private val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)

    @Test
    fun `letting Phoebe to sing Smelly Cat when any guitar string is not tuned returns false (nothing is singed)`() {
        // given
        val array: Array<GuitarString> = arrayOf(GuitarString('a', false), GuitarString('b', true))
        given(phoebe.guitar).willReturn(Guitar(array)) // this is not a complete object structure of Phoebe

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertFalse(didPhoebeSing)
    }

    @Test
    fun `letting Phoebe to sing Smelly Cat when all guitar strings are tuned returns true`() {
        // given
        val array: Array<GuitarString> = arrayOf(GuitarString('a', true), GuitarString('b', true))
        given(phoebe.guitar).willReturn(Guitar(array)) // this is not a complete object structure of Phoebe

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertTrue(didPhoebeSing)
    }
}