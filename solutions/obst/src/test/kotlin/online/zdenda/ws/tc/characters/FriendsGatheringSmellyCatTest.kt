package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class FriendsGatheringSmellyCatTest {

    @Mock
    private lateinit var rachel: Rachel
    @Mock
    private lateinit var monica: Monica
    @Mock
    private lateinit var phoebe: Phoebe
    @Mock
    private lateinit var joey: Joey
    @Mock
    private lateinit var chandler: Chandler
    @Mock
    private lateinit var ross: Ross
    @InjectMocks
    private lateinit var friendsGathering: FriendsGathering

    @Test
    fun `letting Phoebe to sing Smelly Cat when any guitar string is not tuned returns false (nothing is singed)`() {
        // given
        given(phoebe.hasAllGuitarStringsTuned()).willReturn(false)

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertFalse(didPhoebeSing)
    }

    @Test
    fun `letting Phoebe to sing Smelly Cat when all guitar strings are tuned returns true`() {
        // given
        given(phoebe.hasAllGuitarStringsTuned()).willReturn(true)

        // when
        val didPhoebeSing = friendsGathering.letPhoebeSingSmellyCat()

        // then
        assertTrue(didPhoebeSing)
    }
}