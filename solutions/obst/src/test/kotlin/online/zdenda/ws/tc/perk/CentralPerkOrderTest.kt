package online.zdenda.ws.tc.perk

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.given
import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

/**
 * Tests [CentralPerk.order]
 */
@ExtendWith(MockitoExtension::class)
class CentralPerkOrderTest {

    @Mock
    private lateinit var stock: CoffeeStock
    @InjectMocks
    private lateinit var perk: CentralPerk

    @Test
    fun `ordering not offered coffee throws IllegalArgumentException`() {
        // when
        val orderFunction: () -> Unit = { perk.order(CoffeeType.LONG_BLACK) }

        // then
        Assertions.assertThrows(IllegalArgumentException::class.java, orderFunction)
    }

    @Test
    fun `ordering offered coffee with not enough coffee stock returns null`() {
        // given
        given(stock.take(eq(CoffeeType.ESPRESSO), any())).willReturn(false)

        // when
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertNull(coffee)
    }

    @Test
    fun `ordering offered coffee with enough coffee stock returns coffee of such type`() {
        // given
        given(stock.take(eq(CoffeeType.ESPRESSO), any())).willReturn(true)

        // when
        val coffee = perk.order(CoffeeType.ESPRESSO)

        // then
        assertEquals(CoffeeType.ESPRESSO, coffee!!.type)
    }
}