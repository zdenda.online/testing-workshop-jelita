package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

/**
 * Tests [CentralPerk.processComplaint]
 */
@ExtendWith(MockitoExtension::class)
class CentralPerkComplaintsTest {

    @Mock
    private lateinit var stock: CoffeeStock
    @InjectMocks
    private lateinit var perk: CentralPerk

    @Test
    fun `processing complaint of not offered coffee by Central Perk returns false`() {
        // given
        val coffee = aCoffee(type = CoffeeType.LONG_BLACK)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that has not coffee grams equal to the Central Perk's prototype returns true`() {
        // given
        val coffee = aCoffee(type = CoffeeType.ESPRESSO, coffeeGrams = 7, waterMilliliters = 25)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertFalse(isComplaintAccepted)
    }

    @Test
    fun `processing complaint of offered coffee that has coffee grams equal to the Central Perk's prototype returns false`() {
        // given
        val coffee = aCoffee(type = CoffeeType.ESPRESSO, coffeeGrams = 10)

        // when
        val isComplaintAccepted = perk.processComplaint(coffee)

        // then
        assertTrue(isComplaintAccepted)
    }
}