package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.characters.tools.CupsCard
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class FriendsGatheringCupsTest {

    @ParameterizedTest
    @CsvSource(
            "10, SPADE, 3, SPADE, true",
            "3, SPADE, 10, SPADE, true",
            "3, DIAMOND, 3, SPADE, true",
            "3, SPADE, 3, DIAMOND, true",
            "3, DIAMOND, 3, DIAMOND, false")
    fun `playing 'Cups' cards game`(joeyVal: Int, joeyColor: String,
                                    chandlerVal: Int, chandlerColor: String,
                                    expJoeyWins: Boolean) {
        // given
        val rachel: Rachel = mock()
        val monica: Monica = mock()
        val phoebe: Phoebe = mock()
        val joey: Joey = mock()
        val chandler: Chandler = mock()
        val ross: Ross = mock()
        val friendsGathering = FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)

        given(joey.playCupsCard()).willReturn(CupsCard(joeyVal, CupsCard.Color.valueOf(joeyColor)))
        given(chandler.playCupsCard()).willReturn(CupsCard(chandlerVal, CupsCard.Color.valueOf(chandlerColor)))

        // when
        val joeyWins = friendsGathering.playCupsCardsTurn()

        // then
        assertEquals(expJoeyWins, joeyWins)
        verify(joey).playCupsCard()
        verify(chandler).playCupsCard()
    }
}