package online.zdenda.ws.tc.perk

fun aCoffee(
        type: CoffeeType = CoffeeType.ESPRESSO,
        coffeeGrams: Int = 10,
        waterMilliliters: Int = 100,
        milkMilliliters: Int = 0,
        price: Double = 10.0): Coffee {
    return Coffee(type, coffeeGrams, waterMilliliters, milkMilliliters, price)
}