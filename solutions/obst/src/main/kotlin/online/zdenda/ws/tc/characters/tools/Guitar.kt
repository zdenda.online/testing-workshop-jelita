package online.zdenda.ws.tc.characters.tools

class Guitar(val strings: Array<GuitarString>) {

    /**
     * Gets a flag whether all strings of guitar are tuned.
     *
     * @return true if all are tuned, otherwise false
     */
    fun hasAllStringsTuned(): Boolean {
        return strings.all { it.isTuned }
    }
}