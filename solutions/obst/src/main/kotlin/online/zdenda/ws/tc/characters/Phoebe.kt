package online.zdenda.ws.tc.characters

import online.zdenda.ws.tc.characters.tools.Guitar

/**
 * Represents Phoebe Buffay.
 */
class Phoebe(val guitar: Guitar) {

    val smellyCatLyrics = "Smelly Cat, Smelly Cat,\n" +
            "What are they feeding you?\n" +
            "Smelly Cat, Smelly Cat\n" +
            "It's not your fault\n" +
            "\n" +
            "They won't take you to the vet\n" +
            "You're obviously not their favorite pet\n" +
            "You may not be a bed of roses\n" +
            "You're not friend to those with noses"

    /**
     * Gets a flag whether Phoebe is pregnant with his brother's children.
     *
     * @return true if pregnant otherwise false
     */
    fun isPregnant(): Boolean {
        return Math.random() < 0.2
    }

    /**
     * Gets a flag whether all guitar strings of Phoebe are tuned.
     *
     * @return true if all are tuned, otherwise false
     */
    fun hasAllGuitarStringsTuned(): Boolean {
        return guitar.hasAllStringsTuned()
    }
}