package online.zdenda.ws.tc.characters

/**
 * Represents a gathering of all friends somewhere and their interactions.
 */
class FriendsGathering(val rachel: Rachel,
                       val monica: Monica,
                       val phoebe: Phoebe,
                       val joey: Joey,
                       val chandler: Chandler,
                       val ross: Ross) {

    /**
     * Lets Phoebe to sing her famous hit 'Smelly Cat'.
     *
     * @return true if Phoebe singed or false if not.
     */
    fun letPhoebeSingSmellyCat(): Boolean {
        if (phoebe.hasAllGuitarStringsTuned()) {
            println(phoebe.smellyCatLyrics)
            return true
        }
        return false
    }

    /**
     * Attempts to make a couple from Ross and Rachel.
     *
     * @return true if they become couple or false if not.
     */
    fun attemptToCoupleRossAndRachel(): Boolean {
        return ross.isWillingToLoveRachel() && rachel.isWillingToLoveRoss()
    }

    /**
     * Plays a single turn in 'Cups' card game between Joey and Chandler
     *
     * @return true if Joey wins, false if Chandler wins the turn
     */
    fun playCupsCardsTurn(): Boolean {
        val joeyCard = joey.playCupsCard()
        val chandlerCard = chandler.playCupsCard()

        return if (joeyCard.value > chandlerCard.value) {
            true
        } else if (joeyCard.value < chandlerCard.value) {
            true
        } else {
            if (joeyCard.color.ordinal > chandlerCard.color.ordinal) {
                true
            } else {
                joeyCard.color.ordinal < chandlerCard.color.ordinal
            }
        }
    }

    /**
     * Checks whether anyone from the characters did some mess so the Monica should do the cleaning.
     */
    fun checkIfMonicaShouldDoCleaning() {
        val isOttomanMoved = rachel.movedGreenOttoman()
        val isSpaghettiOnFloor = joey.droppedSpaghettiOnTheFloor()
        val isCoffeeTableMessy = ross.marcelPeedInCoffeeTable()
        val areFridgeMagnetsInPlace = chandler.didCleaning()
        val isPhoebePregnant = phoebe.isPregnant() // everything has to be cleaned up - of course
        val doCleaning = isOttomanMoved && isSpaghettiOnFloor && isCoffeeTableMessy && areFridgeMagnetsInPlace && isPhoebePregnant
        if (doCleaning) monica.doCleaning()
    }
}