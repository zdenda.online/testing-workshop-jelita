package online.zdenda.ws.tc.perk.stock

import online.zdenda.ws.tc.perk.CoffeeType
import java.sql.Connection
import java.sql.DriverManager

/**
 * Represents a stock of coffee somewhere in the back of the coffee house.
 * This implementation uses MySQL database for such stock.
 */
class CoffeeStock {

    companion object {
        private const val DB_URL = "jdbc:mysql://localhost/cs"
        private const val DB_USER = "admin"
        private const val DB_PASS = "pass" // wow, what a security hole!
    }

    /**
     * Attempts to take given count of grams of given coffee type from the stock.
     *
     * @return true if there is enough supplies (and they got decremented) or false if not
     */
    fun take(type: CoffeeType, grams: Int): Boolean {
        var connection: Connection? = null
        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS)
            val selectStmt = connection.prepareStatement("SELECT STOCK FROM CS.COFFEE_SUPPLIES WHERE CTYPE = ?")
            selectStmt.setString(1, type.name)
            val rs = selectStmt.executeQuery()

            if (rs.next()) {
                val stock = rs.getInt(1)
                rs.close()

                val decrement = if (stock > grams) grams else stock
                val updateStmt = connection.prepareStatement("UPDATE CS.COFFEE_SUPPLIES SET STOCK = ? WHERE CTYPE = ?")
                updateStmt.setInt(1, stock - decrement)
                updateStmt.setString(2, type.name)

                val updated = updateStmt.execute()
                updateStmt.close()
                return updated
            } else {
                rs.close()
                return false
            }
        } finally {
            connection?.close()
        }
    }

}