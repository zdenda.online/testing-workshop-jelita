package online.zdenda.ws.tc

data class MessOptions(val droppedSpaghettiOnTheFloor: Boolean,
                       val movedGreenOttoman: Boolean,
                       val marcelPeedInCoffeeTable: Boolean,
                       val pheobeIsPregnant: Boolean,
                       val chandlerDidCleaning: Boolean)