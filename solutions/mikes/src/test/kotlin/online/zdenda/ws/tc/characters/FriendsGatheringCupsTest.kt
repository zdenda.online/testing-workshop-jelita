package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.characters.tools.CupsCard
import online.zdenda.ws.tc.aFriendsGathering
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringCupsTest {

    @Test
    fun `playing 'Cups' cards game with Joey getting 10 of spades and Chandler 3 of diamond results in Joey's win`() {
        // given
        val joey: Joey = mock()
        val chandler: Chandler = mock()

        given(joey.playCupsCard()).willReturn(CupsCard(10, CupsCard.Color.SPADE))
        given(chandler.playCupsCard()).willReturn(CupsCard(3, CupsCard.Color.DIAMOND))

        // when
        val joeyWins = aFriendsGathering(joey = joey, chandler = chandler).playCupsCardsTurn()

        // then
        assertTrue(joeyWins)
        verify(joey).playCupsCard()
        verify(chandler).playCupsCard()
    }

    @Test
    fun `playing 'Cups' cards game with Joey getting 10 of spades and Chandler 10 of spades results in Chandler's win`() {
        // given
        val joey: Joey = mock()
        val chandler: Chandler = mock()

        given(joey.playCupsCard()).willReturn(CupsCard(10, CupsCard.Color.SPADE))
        given(chandler.playCupsCard()).willReturn(CupsCard(10, CupsCard.Color.SPADE))

        // when
        val joeyWins = aFriendsGathering(joey = joey, chandler = chandler).playCupsCardsTurn()

        // then
        assertFalse(joeyWins)
        verify(joey).playCupsCard()
        verify(chandler).playCupsCard()
    }

    @Test
    fun `playing 'Cups' cards game with Joey getting same card as Chandler results in Chandler's win`() {
        // given
        val joey: Joey = mock()
        val chandler: Chandler = mock()

        for (color in CupsCard.Color.values()) {
            for(value in 2..15){
                given(joey.playCupsCard()).willReturn(CupsCard(value, color))
                given(chandler.playCupsCard()).willReturn(CupsCard(value, color))

                // when
                val joeyWins = aFriendsGathering(joey = joey, chandler = chandler).playCupsCardsTurn()

                // then
                assertFalse(joeyWins)
            }
        }
    }

    @Test
    fun `playing 'Cups' cards game with Joey getting different card than Chandler results in Joey's win`() {
        // given
        val joey: Joey = mock()
        val chandler: Chandler = mock()

        for (joeysColor in CupsCard.Color.values()) {
            for(joeysValue in 2..15){
                for (chandlersColor in CupsCard.Color.values()) {
                    for(chandlersValue in 2..15){
                        if(joeysColor == chandlersColor && joeysValue == chandlersValue){
                            break
                        }

                        given(joey.playCupsCard()).willReturn(CupsCard(joeysValue, joeysColor))
                        given(chandler.playCupsCard()).willReturn(CupsCard(chandlersValue, chandlersColor))

                        // when
                        val joeyWins = aFriendsGathering(joey = joey, chandler = chandler).playCupsCardsTurn()

                        // then
                        assertTrue(joeyWins)
                    }
                }
            }
        }
    }
}