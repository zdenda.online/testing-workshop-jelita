package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import online.zdenda.ws.tc.aFriendsGathering
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class FriendsGatheringLoveTest {

    @Test
    fun `Ross and Rachel are coupled when both are willing to love each other`() {
        // when
        val coupled = initFriendsGathering(true, true).attemptToCoupleRossAndRachel()

        // then
        assertTrue(coupled)
    }

    @Test
    fun `Ross and Rachel are not coupled when Ross is not willing to love Rachel`() {
        // when
        val coupled = initFriendsGathering(true, false).attemptToCoupleRossAndRachel()

        // then
        assertFalse(coupled)
    }

    @Test
    fun `Ross and Rachel are not coupled when Rachel is not willing to love Ross`() {
        // when
        val coupled = initFriendsGathering(false, true).attemptToCoupleRossAndRachel()

        // then
        assertFalse(coupled)
    }

    @Test
    fun `Probability that Ross is willing to love Rachel is high`(){
        // given
        val ross = Ross()
        var isWillingCount = 0
        val count = 1000

        // when
        for(i in 1..count){
            if(ross.isWillingToLoveRachel()){
                isWillingCount++
            }
        }

        // then
        assertTrue(isWillingCount > count / 2)
        assertTrue(isWillingCount < count)
    }

    @Test
    fun `Probability that Rachel is willing to love Ross is low`(){
        // given
        val rachel = Rachel()
        var isWillingCount = 0
        val count = 1000

        // when
        for(i in 1..count){
            if(rachel.isWillingToLoveRoss()){
                isWillingCount++
            }
        }

        // then
        assertTrue(isWillingCount < count / 2)
        assertTrue(isWillingCount > 1)
    }

    private fun initFriendsGathering(rachelWillingToLoveRoss: Boolean, rossWillingToLoveRachel: Boolean): FriendsGathering {
        val rachel: Rachel = mock()
        val ross: Ross = mock()

        given(rachel.isWillingToLoveRoss()).willReturn(rachelWillingToLoveRoss)
        given(ross.isWillingToLoveRachel()).willReturn(rossWillingToLoveRachel)

        return aFriendsGathering(rachel = rachel, ross = ross)
    }
}