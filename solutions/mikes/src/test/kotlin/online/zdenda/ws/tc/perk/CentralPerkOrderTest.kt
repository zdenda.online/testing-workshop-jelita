package online.zdenda.ws.tc.perk

import online.zdenda.ws.tc.perk.stock.CoffeeStock
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import com.nhaarman.mockito_kotlin.mock
import org.mockito.BDDMockito.given


/**
 * Tests [CentralPerk.order]
 */
class CentralPerkOrderTest {
    @Test
    fun `ordering not offered coffee throws IllegalArgumentException`() {
        // when
        val orderFunction: () -> Unit = { CentralPerk(mockCoffeeStock(true)).order(CoffeeType.LONG_BLACK) }

        // then
        Assertions.assertThrows(IllegalArgumentException::class.java, orderFunction)
    }

    @Test
    fun `ordering offered coffee with not enough coffee stock returns null`() {
        // when
        val coffee = CentralPerk(mockCoffeeStock(false)).order(CoffeeType.ESPRESSO)

        // then
        assertNull(coffee)
    }

    @Test
    fun `ordering offered coffee with enough coffee stock returns coffee of such type`() {
        // when
        val coffee = CentralPerk(mockCoffeeStock(true)).order(CoffeeType.ESPRESSO)

        // then
        assertEquals(CoffeeType.ESPRESSO, coffee!!.type)
    }

    private fun mockCoffeeStock(enoughCoffeeStock : Boolean): CoffeeStock {
        val mock :CoffeeStock= mock()
        given(mock.take(CoffeeType.ESPRESSO, 7)).willReturn(enoughCoffeeStock)
        return mock
    }
}