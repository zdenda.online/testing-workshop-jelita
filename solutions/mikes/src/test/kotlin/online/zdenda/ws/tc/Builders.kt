package online.zdenda.ws.tc

import online.zdenda.ws.tc.characters.*
import online.zdenda.ws.tc.characters.tools.Guitar
import online.zdenda.ws.tc.perk.Coffee
import online.zdenda.ws.tc.perk.CoffeeType

fun aCoffee(
        type: CoffeeType = CoffeeType.ESPRESSO,
        coffeeGrams: Int = 10,
        waterMilliliters: Int = 100,
        milkMilliliters: Int = 0,
        price: Double = 10.0): Coffee {
    return Coffee(type, coffeeGrams, waterMilliliters, milkMilliliters, price)
}

fun aFriendsGathering(
        rachel: Rachel = Rachel(),
        monica: Monica = Monica(),
        phoebe: Phoebe = Phoebe(Guitar(emptyArray())),
        joey: Joey = Joey(),
        chandler: Chandler = Chandler(),
        ross: Ross = Ross()): FriendsGathering{
    return FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
}