package online.zdenda.ws.tc.characters

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import online.zdenda.ws.tc.MessOptions
import org.junit.jupiter.api.Test
import org.mockito.Mockito.never

class FriendsCleaningTest {

    @Test
    fun `Monica did not do cleaning when nobody did mess`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(false, false, false, false, false))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica, never()).doCleaning()
    }

    @Test
    fun `Monica did cleaning when spaghetti dropped on the floor`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(true, false, false, false, false))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica).doCleaning()
    }

    @Test
    fun `Monica did cleaning when ottoman moved`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(false, true, false, false, false))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica).doCleaning()
    }

    @Test
    fun `Monica did cleaning when Marcel peed`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(false, false, true, false, false))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica).doCleaning()
    }

    @Test
    fun `Monica did cleaning when Pheobe is pregnant`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(false, false, false, true, false))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica).doCleaning()
    }

    @Test
    fun `Monica did cleaning when Chandler did cleaning`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(false, false, false, false, true))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica).doCleaning()
    }

    @Test
    fun `Monica did cleaning when mess is all around`() {
        //given
        val friendsGathering = initFriendsGathering(MessOptions(true, true, true, true, true))

        // when
        friendsGathering.checkIfMonicaShouldDoCleaning()

        // then
        verify(friendsGathering.monica).doCleaning()
    }

    private fun initFriendsGathering(messOptions: MessOptions): FriendsGathering {
        val monica: Monica = mock()
        val joey: Joey = mock()
        val rachel: Rachel = mock()
        val ross: Ross = mock()
        val phoebe: Phoebe = mock()
        val chandler: Chandler = mock()

        given(joey.droppedSpaghettiOnTheFloor()).willReturn(messOptions.droppedSpaghettiOnTheFloor)
        given(rachel.movedGreenOttoman()).willReturn(messOptions.movedGreenOttoman)
        given(ross.marcelPeedInCoffeeTable()).willReturn(messOptions.marcelPeedInCoffeeTable)
        given(phoebe.isPregnant()).willReturn(messOptions.pheobeIsPregnant)
        given(chandler.didCleaning()).willReturn(messOptions.chandlerDidCleaning)

        // when
        return FriendsGathering(rachel, monica, phoebe, joey, chandler, ross)
    }
}