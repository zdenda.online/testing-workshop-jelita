General Info
============
* **Solve all tasks in given order** (do not skip or solve two at one time).
* Make a copy of the whole project for yourself = **do NOT overwrite an assignment**
* If you finish your task earlier, help the others :-)

Tasks
=====
1. **Add a new type of coffee - Latte**
    1. add a new enum value `CoffeeType.LATTE`
    2. add a new Coffee attribute `milkMillilitres: Int`
    3. fix tests in `CentralPerkComplaintsTest`
    4. change the tests so it won't be needed to fix them if you add a new property in the future
    5. add a new Coffee attribute `price: Double` 
  
2. **Fix Central Perk ordering tests**
    1. analyze and identify where is the problem for `CentralPerkOrderTest`
    2. fix the test (you may need to change the implementation)
    
4. **Fix 'Smelly Cat' tests of Phoebe**
    1. finish (fix) tests in `FriendsGatheringSmellyCatTest`
    2. find a way to initialize `FriendsGathering` with mocks in other way than CTRL+C and CTRL+V

6. **Add more tests for game of 'Cups' played by Joey and Chandler**
    1. investigate logic of `playCupsCardsTurn()` (there is a high chance Joey wins)
    2. add at least 3 more tests in `FriendsGatheringCupsTest` for other combinations that were not tested yet
    3. find a way to add more such tests without adding a new `@Test` method
        
7. **Add tests for Monica Cleaning**
    1. implement a new test class `FriendsCleaningTest` for `checkIfMonicaShouldDoCleaning` (use Mockito's `verify`)
    2. re-think your test code if it gives you any benefit about the correctness of the production code 
  
3. **Add milk stocks class**
    1. with TDD approach (tests before implementation), implement a new class `MilkStock` that operates in memory
    2. write unit tests for a new `MilkStock`